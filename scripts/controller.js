var robotDirection = nipplejs.create({
    zone: document.getElementById('robotDirection'),
    color: '#ACFF24'
});

var headRotation = nipplejs.create({
    zone: document.getElementById('headRotation'),
    color: '#60F173',
    lockX: true
});

var ip = window.location.search.replace('?ip=', '');
var socket;

function connectSocket() {
    socket = new WebSocket('ws://' + ip + ':81');
}

connectSocket();

document.addEventListener('click', function() {
    if (!document.fullscreenElement) {
        document.documentElement.requestFullscreen();
    }
});

socket.onclose = function(){
    setTimeout(function(){
        connectSocket();
    }, 2000);
};

robotDirection.on('dir:up', function (evt, nipple) {
    socket.send('#DW,0,1,');
    socket.send('#DW,5,1,');
    socket.send('#DW,2,1,');
    socket.send('#DW,4,1,');
});

robotDirection.on('dir:down', function (evt, nipple) {
    socket.send('#DW,0,0,');
    socket.send('#DW,5,1,');
    socket.send('#DW,2,0,');
    socket.send('#DW,4,1,');
});

robotDirection.on('dir:left', function (evt, nipple) {
    socket.send('#DW,0,0,');
    socket.send('#DW,5,1,');
    socket.send('#DW,2,1,');
    socket.send('#DW,4,1,');
});

robotDirection.on('dir:right', function (evt, nipple) {
    socket.send('#DW,0,1,');
    socket.send('#DW,5,1,');
    socket.send('#DW,2,0,');
    socket.send('#DW,4,1,');
});

robotDirection.on('end', function (evt, nipple) {
    socket.send('#DW,0,1,');
    socket.send('#DW,5,0,');
    socket.send('#DW,2,1,');
    socket.send('#DW,4,0,');
});

headRotation.on('move', function (evt, nipple, test) {
    console.log(nipple)
});
